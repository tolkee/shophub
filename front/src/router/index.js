import Vue from "vue";
import VueRouter from "vue-router";
import ShoppingList from "@/views/ShoppingList.vue";
import CreateList from "@/views/CreateList.vue";
import Home from "@/views/Home.vue";

Vue.use(VueRouter);

const routes = [
  { 
    path: '/shoppinglists/:id',
    component: ShoppingList,
  },
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/create",
    name: "create",
    component: CreateList
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
