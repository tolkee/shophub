import Vue from 'vue'
import Vuex from 'vuex'

import app from '@/feathers-client'

Vue.use(Vuex)

export default new Vuex.Store({

   state: {
      currentList: 0,
      items: [],
      selected: [],
      shoppinglists: undefined
   },

   mutations: {
      CREATE_ITEM: function(state, item) {
         if(item.list_id === state.currentList) {
            if (item.checked)
               Vue.set(state.selected, state.selected.length, state.items.length)
            Vue.set(state.items, state.items.length, item)
         }
      },
      UPDATE_ITEM: function(state, item) {
         if(item.list_id === state.currentList) {
            state.items.forEach((it, index) => {
               if (item.id === it.id) {
                  Vue.delete(state.selected, state.selected.indexOf(index))
                  if (item.checked)
                     Vue.set(state.selected, state.selected.length, index)
                  Vue.set(it, "checked", item.checked)
               }
            })
         }
      },

      DELETE_ITEM: function(state, item) {
         if(item.list_id === state.currentList) {
            let indexItemSup
            state.items.forEach((it, index) => {
               if (item.id === it.id) {
                  indexItemSup = index
                  if(item.checked)
                     Vue.delete(state.selected, state.selected.indexOf(index))
                  Vue.delete(state.items, index)
               }
            })

            state.items.forEach((it, index) => {
               if(item.checked && item.id !== it.id && index > indexItemSup)
                  Vue.set(state.selected, state.selected.indexOf(index), index-1)
            })
         }
      },

      CREATE_LIST: function(state, list) {
         if (state.shoppinglists === undefined) state.shoppinglists = {}
         Vue.set(state.shoppinglists, list.id, list)
         console.log(state.shoppinglists);
      },
      PATCH_LIST: function(state, list) {
         Vue.set(state.shoppinglists, list.id, list)
      },
      DELETE_LIST: function(state, list) {
         Vue.delete(state.shoppinglists, list.id)
      },
      SET_CURRENT_LIST: function(state, list_id) {
         state.selected = []
         state.items = []
         state.currentList = Number(list_id)
      },
   },

   actions: {
      FETCH_ITEMS: async function({ commit }, list_id) {
         let itemList
         try {
            itemList = await app.service('items').find({
               query: {
                  list_id: list_id
               }
            })
         } catch (error) {
            console.log(error)
         }
         commit('RESET_CURRENT_ITEMS')
         itemList.forEach(item => {
            commit('CREATE_ITEM', item)
         })
      },
      FETCH_LISTS: async function({ commit }) {
         console.log("fetch");
         let shoppinglists
         try {
            shoppinglists = await app.service('shoppinglists').find()
         } catch (error) {
            console.log(error);
         }
         shoppinglists.data.forEach(shoppinglist => {
            commit('CREATE_LIST', shoppinglist)
         })
      },

   }
})

