const items = require('./items/items.service.js');
const shoppinglists = require('./shoppinglists/shoppinglists.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(items);
  app.configure(shoppinglists);
};
