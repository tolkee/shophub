// Initializes the `shoppinglists` service on path `/shoppinglists`
const { Shoppinglists } = require('./shoppinglists.class');
const createModel = require('../../models/shoppinglists.model');
const hooks = require('./shoppinglists.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/shoppinglists', new Shoppinglists(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('shoppinglists');

  service.hooks(hooks);
};
