const { Service } = require('feathers-knex');

exports.Shoppinglists = class Shoppinglists extends Service {
  constructor(options) {
    super({
      ...options,
      name: 'shoppinglists'
    });
  }
};
