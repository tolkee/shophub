<div align="center">
    <h1>Shophub</h1>
    <p>Made by: Marine Blasius, Rémi Barra, Valentin Hou and Guillaume Lacoste</p>
</div>

## About

This project uses [Feathers](http://feathersjs.com), an open source web framework for building modern real-time applications as well as [Vue.js](https://vuejs.org/), an open source progressive framework for building user interfaces.

Shophub allows you to create and interact with several shopping lists that can be used in a cooperative way by multiple people in real time.

## Project setup

1. Make sure you have [NodeJS](https://nodejs.org/), [npm](https://www.npmjs.com/) and [PostgreSQL](https://www.postgresql.org/) installed

   For postgres, you need to create a db named **shophubDB** with a user named **root** (pwd: **root**).
   Or if you want, you can change the **postgres.connection** (in _/back/config/default.json_) to match your custom user/db.

2. Install your dependencies
   
    ```
    npm install
    ```

3. Start your application via 2 terminals

 - One for the front
    ```
    cd "path"/shophub/front
    npm run-script serve
    ```

 - One for the back
    ```
    cd "path"/shophub/back
    npm run-script dev
    ```

4. Access your application by typing the following URL in any browser: https://localhost:8080